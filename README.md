Test Opentelemetry with aiohttp
[![Python 3.6](https://img.shields.io/badge/python-3.6-blue.svg)](https://www.python.org/downloads/release/python-360/)
===

Here is simples *client* and *server* implementations using aiohttp that emit opentelemetry traces.

Each *client* request to the *server* create trace with a span from the *client* and a span from the *server*.

The *client* make a request every seconds and the *server* wait a second before responding.

Spans are logged into the console and exported toward Jaeger.

## Quick start

Install requirements
```sh
pip install -r requirements.txt
```

Run Jaeger
```sh
docker run --rm --name jaeger -d -p 16686:16686 -p 6831:6831/udp jaegertracing/all-in-one
```

Start the server (default port is 8080)
```sh
python server.py
```

Run the client
```sh
python client.py
```

See the result on [Jaeger](http://localhost:16686).

![Jager result](result.png)

## References

- https://opentelemetry-python.readthedocs.io/en/stable/getting-started.html
- https://opentelemetry-python.readthedocs.io/en/stable/api/trace.html
