import asyncio
import logging

from aiohttp import web
from opentelemetry import trace
from opentelemetry.sdk.trace import TracerProvider
from opentelemetry.sdk.trace.export import ConsoleSpanExporter
from opentelemetry.sdk.trace.export import SimpleExportSpanProcessor
from opentelemetry.ext import jaeger


trace.set_tracer_provider(TracerProvider())
# Log traces in the console
trace.get_tracer_provider().add_span_processor(
    SimpleExportSpanProcessor(ConsoleSpanExporter())
)
# Export to jaeger
jaeger_exporter = jaeger.JaegerSpanExporter(
    service_name="server", agent_host_name="localhost", agent_port=6831
)
trace.get_tracer_provider().add_span_processor(
    SimpleExportSpanProcessor(jaeger_exporter)
)

# ===========================================================================


@web.middleware
async def opentelemetry_middleware(request, handler):
    """ Middleware that retrieve the Ot context from the request header. """
    logging.info(request.headers)
    otheader = request.headers.get('Correlation-Context')
    if otheader is None:
        logging.info("Correlation-Context header not found.")
        return await handler(request)

    ctx = trace.SpanContext(**{
        'is_remote': True,
        **{  # trace_id and value_id
            key: int(value)
            for param in otheader.split(",")
            for key, value in [param.split("=")]
        }
    })
    if not ctx.is_valid():
        logging.warning("Invalid span context.")
        return await handler(request)

    tracer = trace.get_tracer(__name__)
    with tracer.start_as_current_span('server', parent=ctx, kind=trace.SpanKind.SERVER) as span:
        return await handler(request)


async def hello(request):
    """ Sample endpoint. """
    await asyncio.sleep(1)
    return web.json_response({"Hello": "world"})


def init_func():
    """ Web application initialization. """
    app = web.Application(middlewares=[opentelemetry_middleware])
    app.add_routes([web.get('/', hello)])
    return app


def main():
    logging.basicConfig(level="INFO")
    app = init_func()
    web.run_app(app)


if __name__ == '__main__':
    main()
