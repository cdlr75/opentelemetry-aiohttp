import asyncio

import aiohttp
from opentelemetry import trace
from opentelemetry.sdk.trace import TracerProvider
from opentelemetry.sdk.trace.export import ConsoleSpanExporter
from opentelemetry.sdk.trace.export import SimpleExportSpanProcessor
from opentelemetry.ext import jaeger
from opentelemetry.ext.aiohttp_client import create_trace_config


SERVER_URL = "http://localhost:8080"

# ===========================================================================

trace.set_tracer_provider(TracerProvider())
# Log traces in the console
trace.get_tracer_provider().add_span_processor(
    SimpleExportSpanProcessor(ConsoleSpanExporter())
)
# Export to jaeger
jaeger_exporter = jaeger.JaegerSpanExporter(
    service_name="client", agent_host_name="localhost", agent_port=6831
)
trace.get_tracer_provider().add_span_processor(
    SimpleExportSpanProcessor(jaeger_exporter)
)

# ===========================================================================


def get_span_name(params: aiohttp.TraceRequestStartParams):
    return "{params.method} {params.url.path}".format(params=params)


async def do_not_work():
    """ Try to use aiohttp_client integration.
    The Opentelemety header is missing in the client request.
    """
    async with aiohttp.ClientSession(trace_configs=[create_trace_config(
        span_name=get_span_name
    )]) as session:
        while True:
            async with session.get(SERVER_URL) as response:
                print(await response.json())
            await asyncio.sleep(1)

# ===========================================================================


def get_otheader(span):
    """ Header in the client request for Opentelemtry. """
    return {
        "Correlation-Context": f"trace_id={span.context.trace_id},span_id={span.context.span_id}"
    }


async def do_work():
    """ Perform http request to the server with Ot context info in the header. """
    async with aiohttp.ClientSession() as session:
        while True:
            tracer = trace.get_tracer(__name__)
            with tracer.start_as_current_span('hello', kind=trace.SpanKind.CLIENT) as span:
                async with session.get(SERVER_URL, headers=get_otheader(span)) as response:
                    print(await response.json())
                await asyncio.sleep(1)


if __name__ == '__main__':
    loop = asyncio.get_event_loop()
    loop.run_until_complete(do_work())
